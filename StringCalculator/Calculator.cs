﻿using System;
using System.Collections.Generic;

namespace StringCalculator
{
    public class Calculator
    {
        private readonly string customerDelimiterId = "//";
        private readonly string newline = "\n";

        public int Add(string numbers)
        {
            if (string.IsNullOrEmpty(numbers))
            {
                return 0;
            }

            string numbersSection = GetNumberSection(numbers);
            string[] delimiters = GetDelimiters(numbers);
            List<int> numbersList = GetNumbers(numbersSection, delimiters);

            ValidateNumber(numbersList);

            return GetSum(numbersList);
        }

        private void ValidateNumber(List<int> numbersList)
        {
            List<string> negativeNumbers = new List<string>();

            foreach (var number in numbersList)
            {
                if (number < 0)
                {
                    negativeNumbers.Add(number.ToString());
                }
            }

            if (negativeNumbers.Count != 0)
            {
                throw new Exception($"Negative not allowed {string.Join(",", negativeNumbers)}");
            }
        }

        private string[] GetDelimiters(string numbers)
        {
            var multipleCustomDelimiteId = $"{customerDelimiterId}[";
            var multipleCustomDelimiteSeperator = $"]{newline}";
            var multipleCustomDelimiteSplitter = "][";

            if (numbers.StartsWith(multipleCustomDelimiteId))
            {
                var delimiter = numbers.Substring(numbers.IndexOf(multipleCustomDelimiteId) + multipleCustomDelimiteId.Length, numbers.IndexOf(multipleCustomDelimiteSeperator) - (multipleCustomDelimiteSeperator.Length + 1));

                return delimiter.Split(new[] { multipleCustomDelimiteSplitter }, StringSplitOptions.RemoveEmptyEntries);
            }
            else if (numbers.StartsWith(customerDelimiterId))
            {
                return new[] { numbers.Substring(numbers.IndexOf(customerDelimiterId) + customerDelimiterId.Length, numbers.IndexOf(newline) - (newline.Length + 1)) };
            }

            return new[] { ",", newline };
        }

        private string GetNumberSection(string numbers)
        {
            if (numbers.StartsWith(customerDelimiterId))
            {
                return numbers.Substring(numbers.IndexOf(newline) + 1);
            }

            return numbers;
        }

        private int GetSum(List<int> numbersList)
        {
            int sum = 0;

            foreach (var number in numbersList)
            {
                if (number < 1001)
                {
                    sum += number;
                }
            }

            return sum;
        }

        private List<int> GetNumbers(string numbers, string[] delimiters)
        {
            string[] numbersArray = numbers.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            List<int> numbersList = new List<int>();

            foreach (var num in numbersArray)
            {
                if (int.TryParse(num, out int number))
                {
                    numbersList.Add(number);
                }
            }

            return numbersList;
        }
    }
}
